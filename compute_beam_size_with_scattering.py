import numpy as np
import matplotlib.pyplot as plt
from cpymad.madx import Madx
import pandas as pd
from pybt.tools.plotters import *
from madxtools.plot_tool import *
import argparse

# T = 23.08 # GeV kinetic energy
T = 4.54 # GeV kinetic energy ion equivalent of 750 MeV/u

parser = argparse.ArgumentParser(description='Perform a calculation')
parser.add_argument('proton_momentum', type=float, help='Proton momentum for the calculation')
parser.add_argument('air_start_pos', type=float, help='Start position of air in the lattice')
parser.add_argument('air_stop_pos', type=float, help='Stop position of air in the lattice')
parser.add_argument('steps', type=float, help='Step size for the air in the lattice')
args = parser.parse_args()

T = args.proton_momentum
print(f"T = {T}")

E0 = 0.938 # GeV/c^2 proton rest mass
E = T + E0 # total energy
gamma = E / E0 # gamma
beta = np.sqrt(1-gamma**(-2))
p = E*beta # GeV/c beam momentum

# print(f"gamma = {round(gamma,3)}")
# print(f"beta = {round(beta,3)}")
print(f"p = {round(p,3)} GeV/c")

# This function changes calculates the new beta functions and the new emittance
def manipulate_globals_and_return(madx, exn, p, beta, gamma, L, scattering=False):
    madx.globals['betx0'] = 'savebeta_air->betx'
    madx.globals['bety0'] = 'savebeta_air->bety'
    madx.globals['alfx0'] = 'savebeta_air->alfx'
    madx.globals['alfy0'] = 'savebeta_air->alfy'
    madx.globals['dx0'] = 'savebeta_air->dx'
    madx.globals['dy0'] = 'savebeta_air->dy'
    madx.globals['dpx0'] = 'savebeta_air->dpx'
    madx.globals['dpy0'] = 'savebeta_air->dpy'


    if scattering==True:
        
        epsilon_0 = exn/(beta*gamma)

        p_MeV = p*1000 # Beam total energy in MeV
        q = 1
        P = 1.01325 # Standard air pressure at sea level in Bar
        P_Torr = P*750.062 # Standard air pressure at sea level in Torr
        L_rad0 = 301 # For air. Table with radiation lengths: https://cds.cern.ch/record/941314/files/p245.pdf
        L_rad = L_rad0/(P_Torr/760)

        theta_rms = (13.6/p_MeV*beta)*q*np.sqrt(L/L_rad)

        epsilon_1 = epsilon_0*np.sqrt(1 + (madx.globals['betx0']*theta_rms**2)/epsilon_0)
        betx0_1 = madx.globals['betx0'] / (np.sqrt(1 + (madx.globals['betx0']*theta_rms**2)/epsilon_0)) 

        exn = epsilon_1*beta*gamma
        betx0 = betx0_1
        
    else:
        betx0 = madx.globals['betx0']
    bety0 = madx.globals['bety0']
    alfx0 = madx.globals['alfx0']
    alfy0 = madx.globals['alfy0']
    dx0 = madx.globals['dx0']
    dy0 = madx.globals['dy0']
    dpx0 = madx.globals['dpx0']
    dpy0 = madx.globals['dpy0']

    # print(f"betx0 = {betx0}")
    # print(bety0)
    # print(alfx0)
    # print(alfy0)
    # print(dx0)
    # print(dy0)
    # print(dpx0)
    # print(dpy0)

    return betx0, bety0, alfx0, alfy0, dx0, dy0, dpx0, dpy0, exn


air_start_pos = args.air_start_pos
air_stop_pos = args.air_stop_pos
steps = args.steps # meters
inner_marker_array = np.arange(air_start_pos+steps, air_stop_pos, steps)


### Create the initial sequence and run MAD-X

# Matched initial parameters
betx0 = 154.0835045206266
bety0 = 5.222566527078791
alfx0 = -36.90472944993891
alfy0 = 0.2523074897915478
Dx0 = 0.13
Dy0 = 0.0
Dpx0 = 0.02
Dpy0 = 0.0
exn = 7.639770207283603e-06
eyn =  3.534081877201574e-06
sige = 0.000679081344780741

with open('tempfile', 'w') as f:
    madx = Madx(stdout=f,stderr=f)
    madx.option(verbose=True, debug=False, echo=True, warn=True, twiss_print=False)

madx.input('''

QF : QUADRUPOLE, L = 2, APERTYPE=CIRCLE, APERTURE={0.025};
QD : QUADRUPOLE, L = 2, APERTYPE=CIRCLE, APERTURE={0.025};

AIR_START : MARKER;
AIR_END : MARKER;
END: MARKER;

QF1 : QF, K1 := kQF1;
QD2 : QD, K1 := kQD2;
QF3 : QF, K1 := kQF3;
QD4 : QD, K1 := kQD4;

kQF1 = 0.32730047;
kQD2 = -0.36102915;
kQF3 = 0.32789126;
kQD4 = -0.1991137;

simple_seq: SEQUENCE, refer = exit, l = 100;
QF1 : QF1, AT=2;
QD2 : QD2, AT=5;
QF3 : QF3, AT=8;
QD4 : QD4, AT=11;
''')

madx.input(f'''
AIR_START : AIR_START, AT={air_start_pos};
AIR_END : AIR_END, AT={air_stop_pos};

END : END, AT=100;

ENDSEQUENCE;
''')

ex = exn/(beta*gamma)
ex_initial = ex
ey = eyn/(beta*gamma)

madx.command.beam(particle='PROTON',pc=p,ex=ex,ey=ey)
madx.input('BRHO      := BEAM->PC * 3.3356;')
madx.input('SELECT, FLAG=TWISS, COLUMN=NAME,KEYWORD,S,L,K1L,BETX,ALFX,X,DX,PX,DPX,MUX,BETY,ALFY,Y,DY,PY,DPY,MUY,APERTYPE,APER_1,APER_2,KMIN,RE11,RE12,RE21,RE22,RE33,RE34,RE43,RE44,RE16,RE26;')


### Add inner marker at interval between AIR_START and AIR_END markers


madx.input("SEQEDIT, SEQUENCE = simple_seq;")
# Create a for loop for the inner markers:
marker_names = []
for i in range(len(inner_marker_array)):
    name = "INNER_MARKER"+str(i)
    marker_names.append(name)
for count, pos in enumerate(inner_marker_array):
    madx.input(f"INSTALL, ELEMENT=INNER_MARKER{str(count)}, CLASS=MARKER, AT={pos};")
    # print(f"INNER_MARKER{str(count)}")
madx.input("ENDEDIT;")


# Add marker at the end of the line to see the evolution of the beam with fine resolution
madx.input("SEQEDIT, SEQUENCE = simple_seq;")
for count, pos in enumerate(np.arange(air_stop_pos, 100, 1)):
    madx.input(f"INSTALL, ELEMENT=HIDDEN_MARKER{str(count)}, CLASS=MARKER, AT={pos};")
madx.input("ENDEDIT;")



### Run the simulation
madx.use(sequence="simple_seq")
madx.input("SAVEBETA, LABEL=savebeta_air, PlACE = AIR_START, SEQUENCE=simple_seq;")
twiss = madx.twiss(betx=betx0, bety=bety0, alfx=alfx0, alfy=alfy0, Dx=Dx0, Dy=Dy0, Dpx=Dpx0, Dpy=Dpy0).dframe()


### Do the scattering

def process_scattering(madx, exn, beta, gamma, marker_names):

    twiss_list = []
    ex_list = []

    for i in range(len(marker_names)+1):
        # print(i)
        if i == 0:
            # print("SAVEBETA at AIR_START")
            betx0, bety0, alfx0, alfy0, Dx0, Dy0, Dpx0, Dpy0, exn = manipulate_globals_and_return(madx, exn, p, beta, gamma, scattering=False, L=steps) # SAVEBETA at AIR_START
            
            # print(" ")
            # print(f"Extracting: SEQUENCE=simple_seq, FROM=AIR_START, TO=END, NEWNAME=seq_air_start_to_end;")
            madx.input("SEQEDIT, SEQUENCE = simple_seq;")
            madx.input("EXTRACT, SEQUENCE=simple_seq, FROM=AIR_START, TO=END, NEWNAME=seq_air_start_to_end;")
            madx.input("ENDEDIT;")
            
            # print("SAVEBETA at INNER_MARKER0")

            # print(f"MY ex = {ex}")
            madx.command.beam(ex=exn/(beta*gamma), ey=ey)
            ex_list.append(madx.sequence.simple_seq.beam.ex)
            madx.input('BRHO      := BEAM->PC * 3.3356;')

            madx.use(sequence="seq_air_start_to_end")
            madx.input("SAVEBETA, LABEL=savebeta_air, PlACE = INNER_MARKER0, SEQUENCE=seq_air_start_to_end;") # We will SAVEBETA at INNER_MARKER0
            twiss = madx.twiss(betx=betx0, bety=bety0, alfx=alfx0, alfy=alfy0, Dx=Dx0, Dy=Dy0, Dpx=Dpx0, Dpy=Dpy0).dframe() # Input SAVEBETA at AIR_START
            twiss_list.append(twiss)
            # print(twiss.iloc[1].name)
            # print()
            betx0, bety0, alfx0, alfy0, Dx0, Dy0, Dpx0, Dpy0, exn = manipulate_globals_and_return(madx, exn, p, beta, gamma, scattering=True, L=steps)  # SAVEBETA values at INNER_MARKER0 with scattering

            # print("")
            # print(f"Extracting: SEQUENCE=seq_air_start_to_end, FROM=INNER_MARKER0, TO=END, NEWNAME=seq_inner_marker0_to_end;")
            madx.input("SEQEDIT, SEQUENCE = seq_air_start_to_end;")
            madx.input("EXTRACT, SEQUENCE=seq_air_start_to_end, FROM=INNER_MARKER0, TO=END, NEWNAME=seq_inner_marker0_to_end;")
        if (i!=0 and i<len(marker_names)):
            madx.input(f"SEQEDIT, SEQUENCE = seq_inner_marker{str(i-1)}_to_end;")
            # print(f"Extracting: SEQUENCE=seq_inner_marker{str(i-1)}_to_end, FROM=INNER_MARKER{str(i)}, TO=END, NEWNAME=seq_inner_marker{str(i)}_to_end;")
            madx.input(f"EXTRACT, SEQUENCE=seq_inner_marker{str(i-1)}_to_end, FROM=INNER_MARKER{str(i)}, TO=END, NEWNAME=seq_inner_marker{str(i)}_to_end;")
        if (i<len(marker_names)-1):
            madx.input("ENDEDIT;")
            # print(f"SAVEBETA at INNER_MARKER{str(i+1)}")

            madx.command.beam(ex=exn/(beta*gamma),ey=ey)
            ex_list.append(madx.sequence.simple_seq.beam.ex)
            madx.input('BRHO      := BEAM->PC * 3.3356;')

            madx.use(sequence=f"seq_inner_marker{str(i)}_to_end")
            madx.input(f"SAVEBETA, LABEL=savebeta_air, PlACE = INNER_MARKER{str(i+1)}, SEQUENCE=seq_inner_marker{str(i)}_to_end;")
            twiss = madx.twiss(betx=betx0, bety=bety0, alfx=alfx0, alfy=alfy0, Dx=Dx0, Dy=Dy0, Dpx=Dpx0, Dpy=Dpy0).dframe()
            twiss_list.append(twiss)
            # print(twiss.iloc[1].name)
            betx0, bety0, alfx0, alfy0, Dx0, Dy0, Dpx0, Dpy0, exn = manipulate_globals_and_return(madx, exn, p, beta, gamma, scattering=True, L=steps)
            # print("")
        if (i==(len(marker_names)-1)):
            madx.input("ENDEDIT;")
            # print(f"SAVEBETA at INNER_MARKER{str(i+1)}")

            madx.command.beam(ex=exn/(beta*gamma),ey=ey)
            ex_list.append(madx.sequence.simple_seq.beam.ex)
            madx.input('BRHO      := BEAM->PC * 3.3356;')

            madx.use(sequence=f"seq_inner_marker{str(i)}_to_end")
            madx.input(f"SAVEBETA, LABEL=savebeta_air, PlACE = AIR_END, SEQUENCE=seq_inner_marker{str(i)}_to_end;")
            twiss = madx.twiss(betx=betx0, bety=bety0, alfx=alfx0, alfy=alfy0, Dx=Dx0, Dy=Dy0, Dpx=Dpx0, Dpy=Dpy0).dframe()
            twiss_list.append(twiss)
            # print(twiss.iloc[1].name)
            betx0, bety0, alfx0, alfy0, Dx0, Dy0, Dpx0, Dpy0, exn = manipulate_globals_and_return(madx, exn, p, beta, gamma, scattering=True, L=steps)
            # print("")

        # At the end of the loop, we will extract the last sequence
        if i == len(marker_names):
            madx.input(f"SEQEDIT, SEQUENCE = seq_inner_marker{str(i-1)}_to_end;")
            # print(f"Extracting: SEQUENCE=seq_inner_marker{str(i-1)}_to_end, FROM=AIR_END, TO=END, NEWNAME=seq_air_end_to_end;")
            madx.input(f"EXTRACT, SEQUENCE=seq_inner_marker{str(i-1)}_to_end, FROM=AIR_END, TO=END, NEWNAME=seq_air_end_to_end;")
            madx.input("ENDEDIT;")
            # print(f"SAVEBETA at AIR_END")


            madx.command.beam(ex=exn/(beta*gamma),ey=ey)
            ex_list.append(madx.sequence.simple_seq.beam.ex)
            madx.input('BRHO      := BEAM->PC * 3.3356;')

            madx.use(sequence=f"seq_air_end_to_end")
            twiss = madx.twiss(betx=betx0, bety=bety0, alfx=alfx0, alfy=alfy0, Dx=Dx0, Dy=Dy0, Dpx=Dpx0, Dpy=Dpy0).dframe()
            twiss_list.append(twiss)
            # print(twiss.iloc[1].name)
            # print("")

    return twiss_list, ex_list

twiss_list, ex_list = process_scattering(madx, exn, beta, gamma, marker_names)



### Shift the s position

# Shift the s position so that is is coherent.
for i in range(len(twiss_list)):
    twiss_list[i].s = twiss_list[i].s + twiss.loc[twiss_list[i].iloc[1].name].s

### Add the emittances

# We add the emittances to each twiss table
twiss['ex'] = ex_initial

for i in range(len(twiss_list)):
    twiss_list[i]['ex'] = ex_list[i]


### Combine the twiss into a single twiss

# We remove the drifts as they are duplicated names
def remove_drift_rows(df):
    df = df.loc[~df.index.str.startswith('drift')]
    df = df.loc[~df.index.str.startswith('#s')]
    return df

twiss_list = [remove_drift_rows(df) for df in twiss_list]

# We combine all the twiss tables into one
def add_missing_rows(df1, df2):
    # Find rows in df1 with lower 's' than the minimum 's' in df2
    missing_rows = df1[df1['s'] < df2['s'].min()]
    
    # Append these rows to df2 and sort by 's'
    df2_updated = pd.concat([df2, missing_rows]).sort_values(by='s')

    return df2_updated

# Loop through each element in the list
for i in range(1, len(twiss_list)):
    twiss_list[i] = add_missing_rows(twiss_list[i-1], twiss_list[i])

# Finaly, we add the initial twiss table to the beginning
twiss_scattered = add_missing_rows(twiss, twiss_list[-1])


beam_size_end_no_scattered = 1000*beam_size(twiss.loc["end"].betx, twiss.loc["end"].dx, twiss.loc["end"].ex, sige 1)
beam_size_end_scattered = 1000*beam_size(twiss_scattered.loc["end"].betx, twiss_scattered.loc["end"].dx, twiss_scattered.loc["end"].ex, sige, 1)
beam_size_increase = 100*beam_size_end_scattered/beam_size_end_no_scattered - 100

print(f"Beam size at the end is {round(beam_size_end_no_scattered,1)} mm")
print(f"Beam size scattered at the end is {round(beam_size_end_scattered,1)} mm")
print(f"Beam size increase is {round(beam_size_increase)} %")


d = {'p': [p], 'air_start_pos': [air_start_pos], 'air_stop_pos': [air_stop_pos], 'steps': [steps], 'sigmaH': [beam_size_end_no_scattered], 'sigmaH_scattered': [beam_size_end_scattered], 'beam_size_increase': [beam_size_increase]}
df = pd.DataFrame(data=d)

import pickle
import datetime
# strftime is used to format the datetime object as a string in the specified format
filename = 'data/scattering_data_' + datetime.datetime.now().strftime("%Y_%m_%d_%H_%M_%S") + '.pickle'
# filename = 'data/scattering_data_' + str(str(round(p, 3)).replace('.', '_')) + '.pickle'
with open(filename, 'wb') as handle:
    pickle.dump(df, handle, protocol=pickle.HIGHEST_PROTOCOL)

