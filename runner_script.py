# runner_script.py
import subprocess

def run_script(proton_momentum, air_start_pos, air_stop_pos, steps):
    result = subprocess.run(['python', 'compute_beam_size_with_scattering.py', str(proton_momentum), str(air_start_pos), str(air_stop_pos), str(steps)], capture_output=True, text=True)
    print(f'For p = {proton_momentum}, air start pos = {air_start_pos}, air stop pos = {air_stop_pos}, steps = {steps}, output was: {result.stdout}')

def main():
    air_start_pos = 50
    # air_stop_pos = 60
    steps = 5 # meters
    for air_stop_pos in [60]:
        for proton_momentum in [ 5,  6,  7,  8,  9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23]:
            run_script(proton_momentum, air_start_pos, air_stop_pos, steps)

if __name__ == "__main__":
    main()
